
#include <a_samp>

#undef MAX_PLAYERS
#define MAX_PLAYERS (100)

#include <crashdetect> // Zeex/samp-plugin-crashdetect

// YSI Include : pawn-lang/YSI-Includes
#include <YSI\y_timers>
#include <YSI\y_hooks>
#include <YSI\y_va>

#include <whirlpool> // Southclaws/samp-whirlpool
#include <a_mysql> // pBlueG/SA-MP-MySQL 
#include <PAWN.CMD> // urShadow/Pawn.CMD
#include <easyDialog> // Awsomedude/easyDialog

//========================[ DEFINE ]========================
#define GM_HOST_NAME    "GTA Southwood Roleplay | Heavy Roleplay"
#define GM_VERSION      "SW 0.0.1"
//========================[ Modules ]========================

// Utils
#include "includes/utils/colour.pwn"
#include "includes/utils/util.pwn"

// Cores
#include "includes/define.pwn"
#include "includes/enums.pwn"
#include "includes/variables.pwn"
#include "includes/function.pwn"
#include "includes/mysql.pwn"

#include "includes/cores/authenticate.pwn"

// Commands
#include "includes/commands/admin.pwn"
#include "includes/commands/roleplay.pwn"
#include "includes/commands/general.pwn"

main () {
    print("HeavyRP - Created by Leaks");
}

public OnGameModeInit() {

    SendRconCommand("hostname "GM_HOST_NAME"");
    SetGameModeText(GM_VERSION);

    SetNameTagDrawDistance(25.0);
	
	ManualVehicleEngineAndLights();
	DisableInteriorEnterExits();
	EnableStuntBonusForAll(0);

    return 1;
}

public OnPlayerConnect(playerid) {
    PlayerData[playerid][pAdmin] = 0;

	new query[90];
	mysql_format(dbCon, query, sizeof(query), "SELECT COUNT(username) FROM `accounts` WHERE username = '%e'", ReturnPlayerName(playerid));
	mysql_tquery(dbCon, query, "OnPlayerJoin", "d", playerid);
    return 1;
}

public OnPlayerRequestClass(playerid, classid) {
    TogglePlayerSpectating(playerid, true);
    defer ShowLoginCamera(playerid);
    return 1;
}

public OnPlayerSpawn(playerid) {

    TogglePlayerSpectating(playerid, false);
    
    SetPlayerVirtualWorld(playerid, 0);
    SetPlayerInterior(playerid, 0);
    SetPlayerPos(playerid, 198.4090,-107.6075,1.5504);
    SetPlayerFacingAngle(playerid, 86.0094);
    return 1;
}

public OnPlayerText(playerid, text[]) {

    new str[144];

    format(str, sizeof(str), "%s �ٴ���: %s", ReturnRealName(playerid), text);
    ProxDetector(playerid, 20.0, str);

	printf("[%d]%s: %s", playerid, ReturnPlayerName(playerid), text);
	return 0;
}